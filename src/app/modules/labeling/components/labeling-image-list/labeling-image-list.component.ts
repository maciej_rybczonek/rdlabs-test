import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';

import { LabelingAddImage, LabelingRemoveImage, selectImages } from '../../../store/labeling';
import { Picture } from '../../../../shared/models';

@Component({
    selector: 'rdlabs-labeling-image-list',
    templateUrl: './labeling-image-list.component.html'
})

export class LabelingImageListComponent implements OnInit, OnDestroy {

    @ViewChild('uploader', {static: false}) uploader: ElementRef;
    private ngUnsubscribe: Subject<any> = new Subject();
    public images: Picture[];

    constructor(
        private store: Store<any>
    ) { }

    ngOnInit() {
        this.store.select(selectImages).pipe(
            takeUntil(this.ngUnsubscribe)
        ).subscribe(images => {
            this.images = images;
        });
    }

    uploadImage(event) {
        for (let i = 0; i < event.target.files.length; i++) {
            let file = event.target.files[i];
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.store.dispatch(new LabelingAddImage(new Picture({
                    id: uuid(),
                    name: file.name,
                    content: String(reader.result)
                })));
            };
        }
        this.uploader.nativeElement.value = '';
    }

    removeImage(imageId, event) {
        event.preventDefault();
        this.store.dispatch(new LabelingRemoveImage(imageId));
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

}
